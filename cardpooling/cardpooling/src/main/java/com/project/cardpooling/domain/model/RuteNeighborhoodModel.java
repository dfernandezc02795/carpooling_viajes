package com.project.cardpooling.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class RuteNeighborhoodModel {

    private long idRuteNeighborhood;
    private long idRute;
    private long idNeighborhood;
    private String pointMeeting;
    private int position;
}
