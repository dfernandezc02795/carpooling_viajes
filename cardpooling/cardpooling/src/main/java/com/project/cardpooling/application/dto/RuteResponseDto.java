package com.project.cardpooling.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RuteResponseDto {

    private UserDto userDto;
    private String description;
    private int quotas;
}
