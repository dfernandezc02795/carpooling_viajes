package com.project.cardpooling.application.dto;

import com.project.cardpooling.domain.model.RuteNeighborhoodModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelRequestDto {

    private RuteNeighborhoodModel idRute;
    private String schedule;
}
