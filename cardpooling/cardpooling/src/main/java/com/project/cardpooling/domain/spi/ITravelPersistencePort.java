package com.project.cardpooling.domain.spi;

import com.project.cardpooling.domain.model.TravelModel;

import java.util.List;

public interface ITravelPersistencePort {

    TravelModel saveTravel(TravelModel travelModel);

    List<TravelModel> getAllTravels();
}
