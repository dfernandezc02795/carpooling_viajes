package com.project.cardpooling.infrastructure.jpa.mapper;

import com.project.cardpooling.domain.model.NeighborhoodModel;
import com.project.cardpooling.infrastructure.jpa.entity.NeighborhoodEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface NeighbothoodEntityMapper {

    NeighborhoodEntity toEntity(NeighborhoodModel neighborhoodModel);
    NeighborhoodModel toNeighborhood(NeighborhoodEntity neighborhoodEntity);
    List<NeighborhoodModel> toNeighborhoodList(List<NeighborhoodEntity> neighborhoodEntityList);
}
