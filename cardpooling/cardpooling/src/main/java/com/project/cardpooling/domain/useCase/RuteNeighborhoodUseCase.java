package com.project.cardpooling.domain.useCase;


import com.project.cardpooling.domain.api.IRuteNeighborhoodServicePort;
import com.project.cardpooling.domain.model.RuteNeighborhoodModel;
import com.project.cardpooling.domain.spi.IRuteNeighborhoodPersistencePort;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class RuteNeighborhoodUseCase implements IRuteNeighborhoodServicePort {

    public final IRuteNeighborhoodPersistencePort ruteNeighborhoodPersistencePort;

    @Override
    public RuteNeighborhoodModel saveRuteNeighborhood(RuteNeighborhoodModel neighborhoodModel) {
        return ruteNeighborhoodPersistencePort.saveRuteNeighborhood(neighborhoodModel);
    }

    @Override
    public List<RuteNeighborhoodModel> getAllRuteNeighborhood() {
        return ruteNeighborhoodPersistencePort.getAllRuteNeighborhood();
    }
}
