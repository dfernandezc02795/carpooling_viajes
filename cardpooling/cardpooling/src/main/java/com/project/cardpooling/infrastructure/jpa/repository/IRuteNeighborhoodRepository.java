package com.project.cardpooling.infrastructure.jpa.repository;

import com.project.cardpooling.infrastructure.jpa.entity.RuteNeighborhoodEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRuteNeighborhoodRepository extends JpaRepository<RuteNeighborhoodEntity, Long> {
}
