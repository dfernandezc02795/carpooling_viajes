package com.project.cardpooling.application.mapper;

import com.project.cardpooling.application.dto.TravelRequestDto;
import com.project.cardpooling.domain.model.RuteNeighborhoodModel;
import com.project.cardpooling.domain.model.TravelModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface TravelRequestMapper {

    TravelModel toTravel(TravelRequestDto travelRequestDto);

    @Mapping(source = "travelRequestDto.idRute.idRute", target = "idRute")
    @Mapping(source = "travelRequestDto.idRute.idNeighborhood", target = "idNeighborhood")
    @Mapping(source = "travelRequestDto.idRute.pointMeeting", target = "pointMeeting")
    @Mapping(source = "travelRequestDto.idRute.position", target = "position")
    RuteNeighborhoodModel toRuteNeighborhood(TravelRequestDto travelRequestDto);



}
