package com.project.cardpooling.infrastructure.jpa.mapper;

import com.project.cardpooling.domain.model.UserModel;
import com.project.cardpooling.infrastructure.jpa.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface UserEntityMapper {

    UserEntity toEntity(UserModel userModel);
    UserModel toUser(UserEntity userEntity);
    List<UserModel> toUserlList(List<UserEntity> userEntityList);
}
