package com.project.cardpooling.infrastructure.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "ruta")
@AllArgsConstructor
@NoArgsConstructor
public class RuteEntity implements Serializable {

    private static final long serialVersionUID = 4079361666087449913L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ruta")
    private long idRute;

    @Id
    @Column(name = "id_conductor")
    private long idDriver;

    @Column(name = "descripcion")
    private String description;

    @Column(name = "cupos")
    private int quotas;

}
