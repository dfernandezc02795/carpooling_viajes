package com.project.cardpooling.domain.useCase;

import com.project.cardpooling.domain.api.IRuteServicePort;
import com.project.cardpooling.domain.model.RuteModel;
import com.project.cardpooling.domain.spi.IRuteNeighborhoodPersistencePort;
import com.project.cardpooling.domain.spi.IRutePersistencePort;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class RuteUseCase implements IRuteServicePort {

    private final IRutePersistencePort rutePersistencePort;

    @Override
    public void saveRute(RuteModel ruteModel) {
        this.rutePersistencePort.saveRute(ruteModel);
    }

    @Override
    public List<RuteModel> getAllRutes() {
        return this.rutePersistencePort.getAllRutes();
    }
}
