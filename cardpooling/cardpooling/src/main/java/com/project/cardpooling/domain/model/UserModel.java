package com.project.cardpooling.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserModel {

    private long idUser;
    private String name;
    private String lastname;
    private String email;
}
