package com.project.cardpooling.infrastructure.jpa.repository;

import com.project.cardpooling.infrastructure.jpa.entity.NeighborhoodEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface INeighborhoodRepository extends JpaRepository<NeighborhoodEntity, Long> {
}