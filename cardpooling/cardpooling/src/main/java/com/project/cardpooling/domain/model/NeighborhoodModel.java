package com.project.cardpooling.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class NeighborhoodModel {

    private long idNeighborhood;
    private String name;
    private String description;
}
