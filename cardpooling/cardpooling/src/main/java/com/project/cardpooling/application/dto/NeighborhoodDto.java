package com.project.cardpooling.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NeighborhoodDto {

    private String name;
    private String description;
}
