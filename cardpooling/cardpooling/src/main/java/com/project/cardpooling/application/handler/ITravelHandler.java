package com.project.cardpooling.application.handler;

import com.project.cardpooling.application.dto.TravelRequestDto;
import com.project.cardpooling.application.dto.TravelResponseDto;
import com.project.cardpooling.domain.model.TravelModel;

import java.util.List;

public interface ITravelHandler {

    TravelModel saveTravel(TravelRequestDto travelRequest);

    List<TravelResponseDto> getAllTravels();
}
