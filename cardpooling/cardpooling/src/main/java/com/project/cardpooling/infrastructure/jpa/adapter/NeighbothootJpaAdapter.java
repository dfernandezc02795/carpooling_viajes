package com.project.cardpooling.infrastructure.jpa.adapter;

import com.project.cardpooling.domain.model.NeighborhoodModel;
import com.project.cardpooling.domain.spi.INeighborhoodPersistencePort;
import com.project.cardpooling.infrastructure.jpa.entity.NeighborhoodEntity;
import com.project.cardpooling.infrastructure.jpa.exception.NoDataFoundException;
import com.project.cardpooling.infrastructure.jpa.mapper.NeighbothoodEntityMapper;
import com.project.cardpooling.infrastructure.jpa.repository.INeighborhoodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class NeighbothootJpaAdapter implements INeighborhoodPersistencePort {

    public final INeighborhoodRepository neighborhoodRepository;
    public final NeighbothoodEntityMapper neighbothoodEntityMapper;

    @Override
    public NeighborhoodModel saveNeighborhood(NeighborhoodModel neighborhoodModel) {
        return neighbothoodEntityMapper.toNeighborhood(neighborhoodRepository.save(neighbothoodEntityMapper.toEntity(neighborhoodModel)));
    }

    @Override
    public List<NeighborhoodModel> getAllNeighborhoods() {
        List<NeighborhoodEntity> neighborhoodEntityList = neighborhoodRepository.findAll();
        if (neighborhoodEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return neighbothoodEntityMapper.toNeighborhoodList(neighborhoodEntityList);
    }
}
