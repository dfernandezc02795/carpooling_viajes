package com.project.cardpooling.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelResponseDto {

    private RuteNeighborhoodResponseDto idRute;
    private String schedule;
}
