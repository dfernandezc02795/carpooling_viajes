package com.project.cardpooling.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RuteNeighborhoodResponseDto {

    private RuteResponseDto idRute;
    private NeighborhoodDto idNeighborhood;
    private String pointMeeting;
    private int position;
}
