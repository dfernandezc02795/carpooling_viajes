package com.project.cardpooling.domain.useCase;

import com.project.cardpooling.domain.api.INeighborhoodServicePort;
import com.project.cardpooling.domain.model.NeighborhoodModel;
import com.project.cardpooling.domain.spi.INeighborhoodPersistencePort;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class NeighborhoodUseCase implements INeighborhoodServicePort {

    private final INeighborhoodPersistencePort neighborhoodPersistencePort;

    @Override
    public NeighborhoodModel saveNeighborhood(NeighborhoodModel neighborhoodModel) {
        return neighborhoodPersistencePort.saveNeighborhood(neighborhoodModel);
    }

    @Override
    public List<NeighborhoodModel> getAllNeighborhoods() {
        return neighborhoodPersistencePort.getAllNeighborhoods();
    }
}
