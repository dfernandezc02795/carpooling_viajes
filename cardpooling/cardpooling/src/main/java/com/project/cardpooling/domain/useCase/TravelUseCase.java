package com.project.cardpooling.domain.useCase;

import com.project.cardpooling.domain.api.ITravelServicePort;
import com.project.cardpooling.domain.model.TravelModel;
import com.project.cardpooling.domain.spi.ITravelPersistencePort;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class TravelUseCase implements ITravelServicePort {

    public final ITravelPersistencePort travelPersistencePort;

    @Override
    public TravelModel saveTravel(TravelModel travelModel) {
        return travelPersistencePort.saveTravel(travelModel);
    }

    @Override
    public List<TravelModel> getAllTravels() {
        return travelPersistencePort.getAllTravels();
    }
}
