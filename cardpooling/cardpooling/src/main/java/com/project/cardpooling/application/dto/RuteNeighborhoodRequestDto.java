package com.project.cardpooling.application.dto;

import com.project.cardpooling.domain.model.NeighborhoodModel;
import com.project.cardpooling.domain.model.RuteModel;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RuteNeighborhoodRequestDto {

    private RuteModel idRute;
    private NeighborhoodModel idNeighborhood;
    private String pointMeeting;
    private int position;
}
