package com.project.cardpooling.domain.api;

import com.project.cardpooling.domain.model.RuteNeighborhoodModel;

import java.util.List;

public interface IRuteNeighborhoodServicePort {

    RuteNeighborhoodModel saveRuteNeighborhood(RuteNeighborhoodModel neighborhoodModel);

    List<RuteNeighborhoodModel> getAllRuteNeighborhood();
}
