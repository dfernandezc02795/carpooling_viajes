package com.project.cardpooling.infrastructure.jpa.adapter;

import com.project.cardpooling.domain.model.RuteModel;
import com.project.cardpooling.domain.spi.IRutePersistencePort;
import com.project.cardpooling.infrastructure.jpa.entity.RuteEntity;
import com.project.cardpooling.infrastructure.jpa.exception.NoDataFoundException;
import com.project.cardpooling.infrastructure.jpa.mapper.RuteEntityMapper;
import com.project.cardpooling.infrastructure.jpa.repository.IRuteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class RuteJpaAdapter implements IRutePersistencePort {

    public final IRuteRepository ruteRepository;
    public final RuteEntityMapper ruteEntityMapper;

    @Override
    public void saveRute(RuteModel ruteModel) {
        ruteRepository.save(ruteEntityMapper.toEntity(ruteModel));
    }

    @Override
    public List<RuteModel> getAllRutes() {
        List<RuteEntity> ruteEntityList = ruteRepository.findAll();
        if (ruteEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return ruteEntityMapper.toRuteList(ruteEntityList);
    }
}
