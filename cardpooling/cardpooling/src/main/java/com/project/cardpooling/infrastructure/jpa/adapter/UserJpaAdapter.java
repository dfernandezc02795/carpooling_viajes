package com.project.cardpooling.infrastructure.jpa.adapter;

import com.project.cardpooling.domain.model.UserModel;
import com.project.cardpooling.domain.spi.IUserPersistencePort;
import com.project.cardpooling.infrastructure.jpa.entity.UserEntity;
import com.project.cardpooling.infrastructure.jpa.exception.NoDataFoundException;
import com.project.cardpooling.infrastructure.jpa.mapper.UserEntityMapper;
import com.project.cardpooling.infrastructure.jpa.repository.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class UserJpaAdapter implements IUserPersistencePort{

    public final IUserRepository userRepository;
    public final UserEntityMapper userEntityMapper;

    @Override
    public UserModel saveUser(UserModel userModel) {
        return userEntityMapper.toUser(userRepository.save(userEntityMapper.toEntity(userModel)));
    }

    @Override
    public List<UserModel> getAllUsers() {
        List<UserEntity> userEntityList = userRepository.findAll();
        if(userEntityList.isEmpty()){
            throw new NoDataFoundException();
        }
        return userEntityMapper.toUserlList(userEntityList);
    }
}
