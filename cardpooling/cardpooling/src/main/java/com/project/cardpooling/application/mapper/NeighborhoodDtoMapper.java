package com.project.cardpooling.application.mapper;

import com.project.cardpooling.application.dto.NeighborhoodDto;
import com.project.cardpooling.domain.model.NeighborhoodModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface NeighborhoodDtoMapper {

    NeighborhoodDto toDto(NeighborhoodModel neighborhoodModel);
}
