package com.project.cardpooling.domain.spi;

import com.project.cardpooling.domain.model.NeighborhoodModel;

import java.util.List;

public interface INeighborhoodPersistencePort {

    NeighborhoodModel saveNeighborhood(NeighborhoodModel neighborhoodModel);

    List<NeighborhoodModel> getAllNeighborhoods();
}
