package com.project.cardpooling.infrastructure.jpa.mapper;

import com.project.cardpooling.domain.model.RuteNeighborhoodModel;
import com.project.cardpooling.infrastructure.jpa.entity.RuteNeighborhoodEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface RuteNeighborhoodEntityMapper {

    RuteNeighborhoodEntity toEntity(RuteNeighborhoodModel neighborhoodModel);
    RuteNeighborhoodModel toRuteNeighborhood(RuteNeighborhoodEntity ruteNeighborhoodEntity);
    List<RuteNeighborhoodModel> toRuteNeighborhoodList(List<RuteNeighborhoodEntity> ruteNeighborhoodEntityList);
}
