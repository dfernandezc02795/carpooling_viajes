package com.project.cardpooling.domain.api;

import com.project.cardpooling.domain.model.TravelModel;

import java.util.List;

public interface ITravelServicePort {

    TravelModel saveTravel(TravelModel travelModel);

    List<TravelModel> getAllTravels();
}
