package com.project.cardpooling.application.dto;

import com.project.cardpooling.domain.model.UserModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RuteRequestDto {

    private UserModel idUser;
    private String description;
    private int quotas;

}
