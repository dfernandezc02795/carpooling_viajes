package com.project.cardpooling.infrastructure.jpa.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "barrio")
@AllArgsConstructor
@NoArgsConstructor
public class TravelEntity implements Serializable {

    private static final long serialVersionUID = -8781074049100116293L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = " id_viaje")
    private long idTravel;

    @Id
    @Column(name = "id_ruta")
    private long idRute;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @Column(name =  "horario")
    private String schedule;
}
