package com.project.cardpooling.infrastructure.jpa.adapter;

import com.project.cardpooling.domain.model.TravelModel;
import com.project.cardpooling.domain.spi.ITravelPersistencePort;
import com.project.cardpooling.infrastructure.jpa.entity.TravelEntity;
import com.project.cardpooling.infrastructure.jpa.exception.NoDataFoundException;
import com.project.cardpooling.infrastructure.jpa.mapper.TravelEntityMapper;
import com.project.cardpooling.infrastructure.jpa.repository.ITravelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class TravelJpaAdapter implements ITravelPersistencePort {

    public final ITravelRepository travelRepository;
    public final TravelEntityMapper travelEntityMapper;

    @Override
    public TravelModel saveTravel(TravelModel travelModel) {
        return travelEntityMapper.toTravel(travelRepository.save(travelEntityMapper.toEntity(travelModel)));
    }

    @Override
    public List<TravelModel> getAllTravels() {
        List<TravelEntity> travelEntityList = travelRepository.findAll();
        if (travelEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return travelEntityMapper.toTravelList(travelEntityList);
    }
}
