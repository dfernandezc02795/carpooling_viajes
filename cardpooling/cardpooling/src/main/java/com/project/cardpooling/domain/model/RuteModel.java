package com.project.cardpooling.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RuteModel {

    private long idRute;
    private long idDriver;
    private String description;
    private int quotas;
}
