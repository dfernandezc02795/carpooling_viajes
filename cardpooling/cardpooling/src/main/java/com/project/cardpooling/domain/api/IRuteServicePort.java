package com.project.cardpooling.domain.api;

import com.project.cardpooling.domain.model.RuteModel;

import java.util.List;

public interface IRuteServicePort {

    void saveRute(RuteModel ruteModel);

    List<RuteModel> getAllRutes();
}
