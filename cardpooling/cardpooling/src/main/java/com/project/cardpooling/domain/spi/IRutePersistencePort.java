package com.project.cardpooling.domain.spi;

import com.project.cardpooling.domain.model.RuteModel;

import java.util.List;

public interface IRutePersistencePort {

    void saveRute(RuteModel ruteModel);

    List<RuteModel> getAllRutes();
}
