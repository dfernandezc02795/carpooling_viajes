package com.project.cardpooling.domain.api;

import com.project.cardpooling.domain.model.UserModel;

import java.util.List;

public interface IUserServicePort {

    UserModel saveUser(UserModel userModel);

    List<UserModel> getAllUsers();
}
