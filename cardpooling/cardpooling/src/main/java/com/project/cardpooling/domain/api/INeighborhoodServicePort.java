package com.project.cardpooling.domain.api;

import com.project.cardpooling.domain.model.NeighborhoodModel;

import java.util.List;

public interface INeighborhoodServicePort {

    NeighborhoodModel saveNeighborhood(NeighborhoodModel neighborhoodModel);

    List<NeighborhoodModel> getAllNeighborhoods();
}
