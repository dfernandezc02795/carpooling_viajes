package com.project.cardpooling.infrastructure.jpa.adapter;

import com.project.cardpooling.domain.model.RuteNeighborhoodModel;
import com.project.cardpooling.domain.spi.IRuteNeighborhoodPersistencePort;
import com.project.cardpooling.infrastructure.jpa.entity.RuteNeighborhoodEntity;
import com.project.cardpooling.infrastructure.jpa.exception.NoDataFoundException;
import com.project.cardpooling.infrastructure.jpa.mapper.RuteNeighborhoodEntityMapper;
import com.project.cardpooling.infrastructure.jpa.repository.IRuteNeighborhoodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class RuteNeigbothoodJpaAdapter implements IRuteNeighborhoodPersistencePort {

    public final IRuteNeighborhoodRepository ruteNeighborhoodRepository;
    public final RuteNeighborhoodEntityMapper ruteNeighborhoodEntityMapper;

    @Override
    public RuteNeighborhoodModel saveRuteNeighborhood(RuteNeighborhoodModel ruteNeighborhoodModel) {
        return ruteNeighborhoodEntityMapper.toRuteNeighborhood((ruteNeighborhoodRepository.save(ruteNeighborhoodEntityMapper.toEntity(ruteNeighborhoodModel))));
    }

    @Override
    public List<RuteNeighborhoodModel> getAllRuteNeighborhood() {
        List<RuteNeighborhoodEntity> ruteNeighborhoodEntityList = ruteNeighborhoodRepository.findAll();
        if (ruteNeighborhoodEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return ruteNeighborhoodEntityMapper.toRuteNeighborhoodList(ruteNeighborhoodEntityList);
    }
}
