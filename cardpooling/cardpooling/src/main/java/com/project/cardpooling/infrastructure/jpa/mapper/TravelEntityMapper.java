package com.project.cardpooling.infrastructure.jpa.mapper;

import com.project.cardpooling.domain.model.TravelModel;
import com.project.cardpooling.infrastructure.jpa.entity.TravelEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface TravelEntityMapper {

    TravelEntity toEntity(TravelModel travelModel);
    TravelModel toTravel(TravelEntity travelEntity);
    List<TravelModel> toTravelList(List<TravelEntity> travelEntityList);
}
