package com.project.cardpooling.application.mapper;

import com.project.cardpooling.application.dto.UserDto;
import com.project.cardpooling.domain.model.UserModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface UserDtoMapper {

    UserDto toSto(UserModel userModel);
}
