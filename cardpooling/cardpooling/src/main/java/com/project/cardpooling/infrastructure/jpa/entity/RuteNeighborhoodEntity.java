package com.project.cardpooling.infrastructure.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "ruta_barrio")
@AllArgsConstructor
@NoArgsConstructor
public class RuteNeighborhoodEntity implements Serializable {

    private static final long serialVersionUID = -1281212751808769001L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long idRuteNeighborhood;

    @Id
    @Column(name = "id_ruta")
    private long idRute;

    @Id
    @Column(name = "id_barrio")
    private long idNeighborhood;

    @Column(name = "punto_encuentro")
    private String pointMeeting;

    @Column(name = "posicion")
    private int position;
}
