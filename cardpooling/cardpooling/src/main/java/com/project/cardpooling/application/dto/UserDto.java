package com.project.cardpooling.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

    private String name;
    private String lastname;
    private String email;
}
