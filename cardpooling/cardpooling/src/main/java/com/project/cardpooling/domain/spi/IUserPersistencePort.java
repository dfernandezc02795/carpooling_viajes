package com.project.cardpooling.domain.spi;

import com.project.cardpooling.domain.model.UserModel;

import java.util.List;

public interface IUserPersistencePort {

    UserModel saveUser(UserModel userModel);

    List<UserModel> getAllUsers();
}
