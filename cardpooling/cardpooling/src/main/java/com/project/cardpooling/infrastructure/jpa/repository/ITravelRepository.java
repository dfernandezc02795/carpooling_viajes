package com.project.cardpooling.infrastructure.jpa.repository;

import com.project.cardpooling.infrastructure.jpa.entity.TravelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITravelRepository extends JpaRepository<TravelEntity, Long> {
}
