package com.project.cardpooling.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TravelModel {

    private long idTravel;
    private long idRute;
    private String schedule;
}
