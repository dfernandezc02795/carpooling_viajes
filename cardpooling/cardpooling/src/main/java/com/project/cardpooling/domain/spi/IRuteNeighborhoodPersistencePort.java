package com.project.cardpooling.domain.spi;

import com.project.cardpooling.domain.model.RuteNeighborhoodModel;

import java.util.List;

public interface IRuteNeighborhoodPersistencePort {

    RuteNeighborhoodModel saveRuteNeighborhood(RuteNeighborhoodModel neighborhoodModel);

    List<RuteNeighborhoodModel> getAllRuteNeighborhood();
}
