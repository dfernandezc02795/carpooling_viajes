package com.project.cardpooling.infrastructure.jpa.exception;

public class NoDataFoundException extends RuntimeException{
    public NoDataFoundException() {
        super();
    }
}
