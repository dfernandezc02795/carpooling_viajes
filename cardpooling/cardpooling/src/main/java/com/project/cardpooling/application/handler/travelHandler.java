package com.project.cardpooling.application.handler;

import com.project.cardpooling.application.dto.TravelRequestDto;
import com.project.cardpooling.application.dto.TravelResponseDto;
import com.project.cardpooling.application.mapper.TravelRequestMapper;
import com.project.cardpooling.application.mapper.TravelResponseMapper;
import com.project.cardpooling.domain.api.IRuteNeighborhoodServicePort;
import com.project.cardpooling.domain.api.ITravelServicePort;
import com.project.cardpooling.domain.model.TravelModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class travelHandler  implements ITravelHandler{

    private final ITravelServicePort travelServicePort;
    private final IRuteNeighborhoodServicePort ruteNeighborhoodServicePort;
    private final TravelRequestMapper travelRequestMapper;
    private final TravelResponseMapper travelResposeMapper;

    @Override
    public TravelModel saveTravel(TravelRequestDto travelRequest) {
        return null;
    }

    @Override
    public List<TravelResponseDto> getAllTravels() {
        return null;
    }
}
