package com.project.cardpooling.domain.useCase;

import com.project.cardpooling.domain.api.IUserServicePort;
import com.project.cardpooling.domain.model.UserModel;
import com.project.cardpooling.domain.spi.IUserPersistencePort;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class UserUseCase implements IUserServicePort {

    public final IUserPersistencePort userPersistencePort;

    @Override
    public UserModel saveUser(UserModel userModel) {
        return userPersistencePort.saveUser(userModel);
    }

    @Override
    public List<UserModel> getAllUsers() {
        return userPersistencePort.getAllUsers();
    }
}
