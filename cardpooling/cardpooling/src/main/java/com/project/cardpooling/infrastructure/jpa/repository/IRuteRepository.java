package com.project.cardpooling.infrastructure.jpa.repository;

import com.project.cardpooling.infrastructure.jpa.entity.RuteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRuteRepository extends JpaRepository<RuteEntity, Long> {
}
