package com.project.cardpooling.infrastructure.jpa.mapper;

import com.project.cardpooling.domain.model.RuteModel;
import com.project.cardpooling.infrastructure.jpa.entity.RuteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface RuteEntityMapper {

    RuteEntity toEntity(RuteModel ruteModel);
    RuteEntity toRute(RuteEntity ruteEntity);
    List<RuteModel> toRuteList(List<RuteEntity> ruteEntityList);
}
